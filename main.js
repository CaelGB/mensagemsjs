const sentMessages = document.querySelector("#sentMessages");

function sendMessage(){
    const txt = document.querySelector("input").value;
    const message = document.createElement("li");
    message.className = "sent-message"
    message.innerHTML = `<p>${txt}</p>`
    message.appendChild(createButton("deletar", "red", deleteMessage))
    message.appendChild(createButton("Editar", "green", editMessage));
    sentMessages.appendChild(message);
}

function createButton(txt = "", color = "", callback){
    const button = document.createElement("button");
    button.innerText = txt;
    button.style.cssText = `
        background-color: ${color};
        color: white;
        padding: 0.5em;
    `
    button.addEventListener("click", ()=>{
        callback(button);
    });
    return button;
}

function deleteMessage(btn){
    btn.parentNode.remove();
}

function editMessage(){

}